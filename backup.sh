#!/usr/bin/env bash
#
cat /home/dso/.config/alacritty/am

# rm -rf home_dotfiles_old
# mv home_dotfiles home_dotfiles_old
#
# mkdir home_dotfiles
# cd home_dotfiles
# cp /home/dso/.ideavimrc .
# cp /home/dso/.zshenv .
# cp /home/dso/.zshrc .
# cp /home/dso/.ideavimrc ideavimrc
# cp /home/dso/.zshenv zshenv
# cp /home/dso/.zshrc zshrc
#
# mkdir config_dotfiles
# cd config_dotfiles
# cp -r /home/dso/.config/alacritty/ .
# cp -r /home/dso/.config/dunst/ .
# cp -r /home/dso/.config/fuzzel/ .
# cp -r /home/dso/.config/i3/ .
# cp -r /home/dso/.config/kitty/ .
# cp -r /home/dso/.config/rofi/ .
# cp -r /home/dso/.config/sway/ .
# cp -r /home/dso/.config/waybar/ .
# cp -r /home/dso/.config/nvim/ .
# cp -r /home/dso/.config/hypr/ .
# rm -rf nvim/.git
# cd ..
#
# mkdir nixos_conf
# cd nixos_conf
# cp -r /etc/nixos .
