
if [ "$XDG_SESSION_TYPE" = "wayland" ]; then
    export MOZ_ENABLE_WAYLAND=1
    export KITTY_ENABLE_WAYLAND=1
fi

if [ "$DESKTOP_SESSION" = "hyprland" ]; then
    export QT_QPA_PLATFORMTHEME="qt5ct"
fi

if [ "$DESKTOP_SESSION" = "sway" ]; then
    export QT_QPA_PLATFORMTHEME="qt5ct"
fi

export ANDROID_HOME="/home/dso/Android/Sdk"
