
# Lines configured by zsh-newuser-install
HISTFILE=~/.histfile
HISTSIZE=1000
SAVEHIST=1000
setopt autocd extendedglob nomatch notify
bindkey -v
# End of lines configured by zsh-newuser-install
# The following lines were added by compinstall
zstyle :compinstall filename '/home/dso/.zshrc'

autoload -Uz compinit
compinit
# End of lines added by compinstall

export ZSH="$HOME/.oh-my-zsh"


ZSH_THEME="robbyrussell"
plugins=(git colored-man-pages)

source $ZSH/oh-my-zsh.sh
#export ANDROID_HOME=/home/dso/Android/cmdsdk/cmdline-tools/latest-2

#export PATH=/opt/flutter/bin/:$PATH
#export PATH=/home/dso/Android/cmdsdk/cmdline-tools/latest-2/bin:$PATH

alias pman="sudo pacman"
alias n="nvim"
alias vim="nvim ."
# alias l="lsd -l --sort extension"
# alias l="lsd -l --group-directories-first"
# alias ll="lsd -l --group-directories-first --size bytes"
alias l="exa --icons -l --group-directories-first"
alias ll="exa --icons -l --group-directories-first --bytes"
alias cat="bat"
# alias ls="lsd -a"
# alias fzf="fzf --preview 'bat --color=always --style=numbers --line-range=:500 {}'"
alias ..="cd .."
alias gl="git log --graph --all --abbrev-commit"
alias b="btop"
alias :q="cd .."
alias android_emulator="/home/dso/Android/Sdk/emulator/emulator"

function c() {
    clear
    exa --icons -l --group-directories-first
}

function ac() {
    git add .
    git commit
}

function acp() {
    git add .
    git commit
    git push
}

function updateAll() {
    sudo pacman -Suy
    paru
    flatpak update
}

function backup() {
    sh ~/MINE/dotbackup/backup.sh
}

function MINE() {
    cd /home/dso/MINE/
}

function vuz() {
    cd /home/dso/MINE/vuz/
}

function android_workspace(){
    cd /home/dso/MINE/android/workspace/
}

function kotlin_workspace(){
    cd /home/dso/MINE/kotlin/workspace/
}

function go_workspace(){
    cd /home/dso/MINE/go/workspace/
}

function rust_workspace() {
    cd /home/dso/MINE/rust/workspace/
}

function ts_workspace() {
    cd /home/dso/MINE/typescript/workspace/
}

function flutter_workspace() {
    cd /home/dso/MINE/flutter/workspace/
}

function nvim_config(){
    cd /home/dso/.config/nvim/
}

function kitty_config(){
    cd /home/dso/.config/kitty/
}

function scan_ips() {
    arp-scan --interface=wlan0 --localnet
} 

# eval "$(starship init zsh)"
bindkey -v

#THIS MUST BE AT THE END OF THE FILE FOR SDKMAN TO WORK!!!
export SDKMAN_DIR="$HOME/.sdkman"
[[ -s "$HOME/.sdkman/bin/sdkman-init.sh" ]] && source "$HOME/.sdkman/bin/sdkman-init.sh"
