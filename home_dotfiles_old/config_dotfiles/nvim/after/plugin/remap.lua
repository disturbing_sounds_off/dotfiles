
vim.keymap.set("v", "K", ":m '<-2<CR>gv=gv")
vim.keymap.set("v", "J", ":m '>+1<cr>gv=gv")


vim.keymap.set("n", "n", "nzzzv")
vim.keymap.set("n", "N", "Nzzzv")

vim.keymap.set("n", "<leader>p", "\"+p")
vim.keymap.set("n", "<leader>з", "\"+p")
vim.keymap.set("v", "<leader>p", "\"_dPj")

vim.keymap.set("n", "<leader>y", "viw\"+y")
vim.keymap.set("v", "<leader>y", "\"+y")
vim.keymap.set("v", "<leader>н", "\"+y")
vim.keymap.set("n", "<leader>Y", "\"+Y")

vim.keymap.set("n", "<leader>d", "\"_d")
vim.keymap.set("v", "<leader>d", "\"_d")

vim.keymap.set("i", "<C-c>", "<Esc>")
vim.keymap.set("i", "<C-с>", "<Esc>")


vim.keymap.set("n", "<leader>(", "va(V")
vim.keymap.set("n", "<leader>{", "va{V")

vim.keymap.set("n", "<M-p>", vim.cmd.Ex)
vim.keymap.set("n", "<leader>e", vim.cmd.Ex)
vim.keymap.set("n", "<leader>у", vim.cmd.Ex)

vim.keymap.set("n", "<leader>r", ":%s/\\<<C-r><C-w>\\>/<C-r><C-w>/gI<Left><Left><Left>")

