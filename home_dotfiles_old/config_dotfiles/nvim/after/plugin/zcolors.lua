function colorMyPencils(color)
    color = color or "dracula"
    -- color = color or "tokyonight-night"
    -- color = color or "darcula"
    -- color = color or "kanagawa"
    -- color = color or "rose-pine-moon"
    -- color = color or "catppuccin-mocha"
    vim.cmd.colorscheme(color)
    -- vim.cmd('colorscheme rose-pine')

    -- transparant

    -- vim.api.nvim_set_hl(0, "Normal", {bg = "none"})
    -- vim.api.nvim_set_hl(0, "NormalFloat", {bg = "none"})
end

colorMyPencils()
