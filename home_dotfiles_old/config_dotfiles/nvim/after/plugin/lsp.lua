local lsp = require('lsp-zero').preset({
    name = 'minimal',
    set_lsp_keymaps = true,
    manage_nvim_cmp = true,
    suggest_lsp_servers = true,
})

local cmp = require('cmp')
local cmp_action = require('lsp-zero').cmp_action()

lsp.setup_nvim_cmp({
    mapping = lsp.defaults.cmp_mappings({
        ['<C-Space>'] = cmp_action.toggle_completion(),
        ['<C-e>'] = cmp.mapping.abort(),
        -- ['<Tab>'] = vim.NIL,
    })
})

lsp.on_attach(function(client, bufnr)
    lsp.default_keymaps({ buffer = bufnr })
    lsp.buffer_autoformat()
end)


lsp.setup()

vim.diagnostic.config()


vim.diagnostic.config({
    virtual_text = true
})
