# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, ... }:

{
    imports =
        [ # Include the results of the hardware scan.
        ./hardware-configuration.nix
        ];

# Bootloader.
    boot.loader.systemd-boot.enable = true;
    boot.loader.efi.canTouchEfiVariables = true;

    networking.hostName = "dsonix"; # Define your hostname.
# networking.wireless.enable = true;  # Enables wireless support via wpa_supplicant.

# Configure network proxy if necessary
# networking.proxy.default = "http://user:password@proxy:port/";
# networking.proxy.noProxy = "127.0.0.1,localhost,internal.domain";

# Enable networking
    networking.networkmanager.enable = true;

# Set your time zone.
    time.timeZone = "Europe/Moscow";

# Select internationalisation properties.
    i18n.defaultLocale = "en_US.UTF-8";

    i18n.extraLocaleSettings = {
        LC_ADDRESS = "ru_RU.UTF-8";
        LC_IDENTIFICATION = "ru_RU.UTF-8";
        LC_MEASUREMENT = "ru_RU.UTF-8";
        LC_MONETARY = "ru_RU.UTF-8";
        LC_NAME = "ru_RU.UTF-8";
        LC_NUMERIC = "ru_RU.UTF-8";
        LC_PAPER = "ru_RU.UTF-8";
        LC_TELEPHONE = "ru_RU.UTF-8";
        LC_TIME = "ru_RU.UTF-8";
    };

# Enable the X11 windowing system.
    services.xserver.enable = true;

# Enable the KDE Plasma Desktop Environment.
    services.xserver.displayManager.sddm.enable = true;
    services.xserver.desktopManager.plasma5.enable = true;
    
    # to run non nix binaries ?
    # https://github.com/Mic92/nix-ld
    # https://nix.dev/guides/faq.html
    programs.nix-ld.enable = true;
    programs.nix-ld.libraries = with pkgs; [
        # Add any missing dynamic libraries for unpackaged programs
        # here, NOT in environment.systemPackages
    ];

# Enable Hyprland and hyprland stuff
    programs.hyprland.enable = true;
    programs.waybar = {
        enable = true;
        package = pkgs.waybar.overrideAttrs (oldAttrs: {
            mesonFlags = oldAttrs.mesonFlags ++ [ "-Dexperimental=true" ];
        });
    };

    programs.zsh = {
        enable = true;
    };  

    programs.git = {
        enable = true;
        lfs.enable = true;
    };
# programs.steam = {
#   enable = true;
#   remotePlay.openFirewall = true; # Open ports in the firewall for Steam Remote Play
#   dedicatedServer.openFirewall = true; # Open ports in the firewall for Source Dedicated Server
# };

    virtualisation.virtualbox.host.enable = true;
    virtualisation.docker.enable = true;
    users.extraGroups.vboxusers.members = [ "dso" ];

    fonts.fontDir.enable = true;

# nixpkgs.config.allowUnfreePredicate = pkg:
#   builtins.elem (lib.getName pkg) [ "corefonts" ];

    fonts.packages = with pkgs; [
        (nerdfonts.override { 
         fonts = [
         "Agave" 
         "LiberationMono" 
         "Cousine"
         "JetBrainsMono"
         "IBMPlexMono"
         ]; 
         })
    font-awesome
        corefonts
        vistafonts
    ];

    users.defaultUserShell = pkgs.zsh;
    environment.shells = with pkgs; [ zsh ];

# For bluetooth
    services.blueman.enable = true;

# Flatpaks
    services.flatpak.enable = true;

# Configure keymap in X11
    services.xserver = {
        layout = "us";
        xkbVariant = "";
    };

# Enable polkit ?
    security.polkit.enable = true;

# Kde partition manager
    programs.partition-manager.enable = true;

# Enable CUPS to print documents.
    services.printing.enable = true;

# Enable sound with pipewire.
    sound.enable = true;
    hardware.pulseaudio.enable = false;
    security.rtkit.enable = true;
    services.pipewire = {
        enable = true;
        alsa.enable = true;
        alsa.support32Bit = true;
        pulse.enable = true;
# If you want to use JACK applications, uncomment this
#jack.enable = true;

# use the example session manager (no others are packaged yet so this is enabled by default,
# no need to redefine it in your config for now)
#media-session.enable = true;
    };

# Enable touchpad support (enabled default in most desktopManager).
# services.xserver.libinput.enable = true;

# Define a user account. Don't forget to set a password with ‘passwd’.
    users.users.dso = {
        isNormalUser = true;
        description = "dso";
        extraGroups = [ "networkmanager" "wheel" "dso"];
        packages = with pkgs; [
            firefox
                kate
#  thunderbird
        ];
    };

# Allow unfree packages
    nixpkgs.config.allowUnfree = true;

# List packages installed in system profile. To search, run:
# $ nix search wget
        # Add any missing dynamic libraries for unpackaged programs
        # here, NOT in environment.systemPackages
    environment.systemPackages = with pkgs; [
        vim 
            wget
            brave
            obs-studio
            eza
            bat
            neofetch
            pfetch
            rar
            unrar
            zip
            unzip
            heroic
            ripgrep
            tor-browser-bundle-bin
            wl-clipboard
# wl-clipboard-x11
            xclip
            tree 
            vlc
            which
            btop
            fzf
            mlocate
            telegram-desktop
            qbittorrent
            xdg-utils
            tldr
            libsForQt5.qtstyleplugin-kvantum
            libsForQt5.qt5ct
            lxappearance
            protonup-qt
            bottles
# partition-manager
# gparted
            libsForQt5.kdenlive

# FOR HYPRLAND
            flameshot
            dunst
            hyprpaper
            fuzzel
            hyprpicker
            sway-contrib.grimshot # REMOVE 
            grimblast
            swappy
# nstable.satty
            playerctl
            pulseaudio
            brightnessctl
            hyprland-protocols
            networkmanagerapplet
            libsForQt5.polkit-kde-agent
# END FOR HYPRLAND

            alacritty

# shit needs to work
# libgcc
            gcc
            pavucontrol
            wlr-randr
            pasystray
            distrobox
            appimage-run
            fd

            # i need to work too
            # android-studio
            android-tools
            # jetbrains.idea-community
            # jetbrains-toolbox
            go
            flyctl
            sqlite
            litecli
            postgresql
            nodejs_20            
            bun
            scrcpy
            tmux
            vscodium-fhs
            # figma-linux
            wlsunset
            neovim
            eyedropper
            onlyoffice-bin
            webcord
            lxqt.pcmanfm-qt
            drawio
            hyperfine

# emulation
            yuzu-mainline

# test
# unstable.pfetch
            ];
# }

            services.dbus.enable = true;
            xdg.portal = {
                enable = true;
                wlr.enable = true;
                extraPortals = [pkgs.xdg-desktop-portal-gtk];
            };

# Some programs need SUID wrappers, can be configured further or are
# started in user sessions.
# programs.mtr.enable = true;
# programs.gnupg.agent = {
#   enable = true;
#   enableSSHSupport = true;
# };

# List services that you want to enable:

# Enable the OpenSSH daemon.
services.openssh.enable = true;

# Open ports in the firewall.
# networking.firewall.allowedTCPPorts = [ ... ];
# networking.firewall.allowedUDPPorts = [ ... ];
# Or disable the firewall altogether.
# networking.firewall.enable = false;

# This value determines the NixOS release from which the default
# settings for stateful data, like file locations and database versions
# on your system were taken. It‘s perfectly fine and recommended to leave
# this value at the release version of the first install of this system.
# Before changing this value read the documentation for this option
# (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
system.stateVersion = "23.11"; # Did you read the comment?
###
}
